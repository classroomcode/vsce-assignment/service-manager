export PATH := ${HOME}/.local/bin:${PATH}

# Install in .local
all:
	python3 setup.py develop --user

# Install in extension environment
asgmt:
	python3 setup.py develop

# Rules below create test fixture
tmp:
	mkdir $@

tmp/minimal-vsce-microservice: tmp
	cd tmp && git clone https://gitlab.com/classroomcode/minimal-vsce-microservice

env:
	pip3 install virtualenv --user
	virtualenv $@

setup_test: tmp/minimal-vsce-microservice env
	. env/bin/activate && cd $^ && make

test: setup_test
	$(MAKE)
	python3 tests/test_service_manager.py

.PHONY: clean
clean:
	rm -rf tmp
	rm -rf env
	pip3 uninstall -y VSCE_ServiceManager
	rm -rf VSCE_ServiceManager.egg-info