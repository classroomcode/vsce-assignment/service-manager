from collections import defaultdict
import os
from typing import DefaultDict, Dict, List, Set, Union
from websockets.client import WebSocketClientProtocol
import VSCE_ServiceManager
import logging
import websockets
import asyncio
import pickle
import threading

logger = logging.getLogger(__name__)

class LogBroadcastService(logging.Handler):
    """
    Broadcast logs to subscribers
    """
    
    def __init__(self):
        super().__init__()
        # A client is a log record consumer
        self.clients: Set[WebSocketClientProtocol] = set()
        # A assignment_client only consumes messages from services of that 
        # assignment
        self.assignment_clients: DefaultDict[
            os.PathLike,
            Set[WebSocketClientProtocol]] = defaultdict(set)
        
        # A service is a log record producer
        self.servers: Set[WebSocketClientProtocol] = set()

        self.loop = asyncio.new_event_loop()
        def thrd_start():
            asyncio.set_event_loop(self.loop)
            self.loop.run_until_complete(
                websockets.serve(
                    self.ws_handler,
                    "localhost",
                    VSCE_ServiceManager.LOG_BROADCAST_PORT
                )
            )
            self.loop.run_forever()
        threading.Thread(target=thrd_start, daemon=True).start()

    def emit(self, record: logging.LogRecord):
        record.exc_info = None
        data = pickle.dumps(record)
        # Only Loggers @ 'all' may receive logs from the service manager itself
        for client in self.clients:
            asyncio.run_coroutine_threadsafe(
                client.send(data),
                self.loop,
            )

    async def ws_handler(self, ws, *args):
        asgmt: str = None
        asgmt_path: str = None
        dispatcher = None
        message: str

        try:
            async for message in ws:
                if isinstance(message, str):
                    if message.startswith("client:"):
                        asgmt = message.replace("client:", "").strip()
                        if asgmt == "all":
                            self.clients.add(ws)
                        else:
                            self.assignment_clients[asgmt].add(ws)
                        logger.warn(f"Logger @ '{asgmt}' attached")
                    
                    elif message.startswith("service:"):
                        asgmt_path = message.replace("service:", "").strip()
                        if asgmt_path == "all":
                            # Bind 'dispatcher' to functions
                            async def dispatcher(msg):
                                for client in self.clients:
                                    await client.send(msg)
                        else:
                            async def dispatcher(msg):
                                # Send to just the subscribers
                                for client in self.assignment_clients[asgmt_path]:
                                    await client.send(msg)
                                # Send to all
                                for client in self.clients:
                                    await client.send()
                        logger.warn(f"Service @ '{asgmt_path}' attached")

                elif isinstance(message, bytes):
                    # If the dispatcher is None, then the client did not register
                    # itself as a service.
                    assert dispatcher is not None
                    # Otherwise, forward the service
                    await dispatcher(message)
        finally:
            if ws in self.clients:
                assert asgmt == "all"
                self.clients.remove(ws)
                logger.warn("Logger @ 'all' detached")

            elif asgmt_path is not None:
                assert asgmt is None
                assert dispatcher is not None
                logger.warn(f"Service @ '{asgmt_path}' detached")
            else:
                assert asgmt is not None
                assert asgmt in self.assignment_clients
                assert ws in self.assignment_clients[asgmt]
                self.assignment_clients[asgmt].remove(ws)
                logger.warn(f"Logger @ {asgmt} detached")