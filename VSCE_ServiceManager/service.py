from VSCE_ServiceManager import LOG_BROADCAST_PORT, PORT, kill_listener, port_empty
import subprocess
import time
from typing import Set
import os
import threading
import logging

logger = logging.getLogger(__name__)
logger.setLevel(1)

class Service:
    """
    Reference counted Service
    Args:
        directory: Directory from which to run service
        command: The command to run
        timeout: Wait duration for the process to be addressable
    """
    user_ports: Set[str] = {
        str(n) for n in range(1024, 49151) if n not in {PORT, LOG_BROADCAST_PORT}
    }

    def __init__(self, directory: os.PathLike, command: str, timeout: int = 120):
        logger.warning(f"Starting service @ {directory}")
        curdir = os.getcwd()
        self.port = self.user_ports.pop()
        self.cond = threading.Condition()
        self._pid = None

        while not port_empty(self.port):
            self.port = self.user_ports.pop()
        
        try:
            os.chdir(directory)
            self.process = subprocess.Popen(
                command + f" localhost {self.port}",
                shell=True,
                stderr=subprocess.DEVNULL,
                stdout=subprocess.DEVNULL,
            )
            while timeout > 0:
                if not port_empty(self.port):
                    break
                time.sleep(0.25)
                timeout -= 0.25 # type: ignore
            else:
                raise Exception("Service failed to start")
            self.refcnt = 1
        finally:
            os.chdir(curdir)

    @property
    def pid(self):
        if self._pid is None:
            result = subprocess.check_output(
                f"lsof -i:{self.port} | awk '{{print $2}}'",
                shell=True,
                encoding="utf-8"
            )
            if result:
                self._pid = int(result)
        return self._pid

    def incref(self) -> int:
        """
        Increments the reference count
        
        Returns:
            The reference count after it has been incremented
        """
        with self.cond:
            self.refcnt += 1
        return self.refcnt

    def decref(self) -> int:
        """
        Decrement the reference count
        
        Returns:
            The reference count after it has been decremented
        """
        with self.cond:
            self.refcnt -= 1
        return self.refcnt

    def terminate(self, force=False, timeout=10, on_success=lambda:None):
        """Terminate the process
        Args:
            force: If true, kill the service even if dependencies are still alive
            timeout: Revive process if a new request was received within timeout
            on_success: A callable to be executed after the process was killed
        """

        def kill_this():
            kill_listener(self.port)
            while not port_empty(self.port):
                time.sleep(0.5)
            self.user_ports.add(self.port)
            logger.warn(f"Killed service listening on port {self.port}")
            on_success()
        
        if force:
            kill_this()

        elif self.refcnt > 0:
            raise Exception("Can't kills service yet")
        else:
            def thrd_target():
                # If a workspace reloads, then service stop, start request are
                # happen within a few seconds of each other. Instead of 
                # immediately killing the service, wait a bit to see if
                # refcnt is incremented.
                with self.cond:
                    self.cond.wait(timeout)
                    if self.refcnt <= 0:
                        kill_this()
                    else:
                        logger.info(f"Service at {self.port} revived")
            threading.Thread(target=thrd_target).start()
            