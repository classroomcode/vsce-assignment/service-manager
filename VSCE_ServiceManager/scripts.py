# Helper scripts to be executed via command line
from logging import LogRecord
from os import kill
from VSCE_ServiceManager import PORT, LOG_BROADCAST_PORT, kill_listener, port_empty
import argparse
import requests
import json
import time
import signal


def exec_service_start_request():    
    parser = parse_args(description="Start VSCE-Assignment Service")
    parser.add_argument("service_dir", help="Absolute path to source folder")
    parser.add_argument("command", help="Run command in shell")
    args = parser.parse_args()

    body = {
        "jsonrpc": "2.0",
        "id": -1,
        "method": "start",
        "params": [
            [   # pos args
                args.assignment,
                args.service_url,
                args.service_dir,
                args.command,
            ],
            {}
        ]
    }
    try:
        req = requests.get(f"http://localhost:{PORT}/process", json=body)
    except requests.exceptions.ConnectionError:
        return 1
    
    if req.status_code != 200:
        return 2
    print(json.dumps(req.json(), indent=4))
    return 0


def exec_service_stop_request():
    parser = parse_args(description="Stop VSCE-Assignment Service")
    parser.add_argument("-f", "--force",
        action="store_true", 
        help="Immediately terminate the service")

    args = parser.parse_args()
    body = {
        "jsonrpc": "2.0",
        "id": -1,
        "method": "stop",
        "params": [[args.assignment, args.service_url], {"force": args.force}]
    }
    try:
        req = requests.get(f"http://localhost:{PORT}/process", json=body)
    except requests.exceptions.ConnectionError:
        return 1
    if req.status_code != 200:
        return 2
    
    print(json.dumps(req.json(), indent=4))
    return 0


def parse_args(*args, **kwargs):
    parser = argparse.ArgumentParser(*args, **kwargs)
    parser.add_argument("assignment", help="Absolute path to assignment directory")
    parser.add_argument("service_url", help="URL of service's source repository")
    return parser


def exec_service_manager():
    import os
    from VSCE_ServiceManager.main import main

    if os.fork() == 0:
        os.chdir("/")
        os.setsid()
        os.umask(0)
        main()

    
def exec_service_logs():
    import websockets
    import asyncio
    import pickle
    import logging
    import os

    parser = argparse.ArgumentParser(
        description="Subscribe to service manager log feed"
    )
    parser.add_argument(
        "-a", "--all",
        action="store_true",
        help="subscribe to all logs")
    parser.add_argument(
        "-p", "--persistent",
        action="store_true",
        help="listen for service manager",
    )
    parser.add_argument("--level",
        choices=["DEBUG", "INFO", "WARN", "CRITICAL", "ERROR"],
        default="WARN",
        help="logging level"
    )
    parser.add_argument(
        "--format",
        default="%(asctime)s %(process)d %(message)s",
        help="logging format",
    )
    args = parser.parse_args()
    logger = logging.getLogger(__name__)
    logger.setLevel(getattr(logging, args.level))
    handler = logging.StreamHandler()
    handler.setFormatter(logging.Formatter(args.format))
    logger.addHandler(handler)
    
    init_arg = "client:"
    if args.all:
        init_arg += "all"
    elif not os.path.exists(".vsce"):
        parser.print_help()
        return 1
    else:
        init_arg += os.getcwd()
    
    async def log():
        async with websockets.connect(
            f"ws://localhost:{LOG_BROADCAST_PORT}"
        ) as ws:
            await ws.send(init_arg)
            while True:
                logrecord = pickle.loads(await ws.recv())
                logger.handle(logrecord)

    if not args.persistent:
        try:
            asyncio.run(log())
        except KeyboardInterrupt:
            return 0
        return 1

    loop = asyncio.get_event_loop()
    def restart(_task):
        nonlocal task
        # Retrieve the exception otherwise asyncio will complain
        _task.exception()
        time.sleep(1)
        task = loop.create_task(log())
        task.add_done_callback(restart)

    task = loop.create_task(log())
    task.add_done_callback(restart)
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        return 0

def exec_service_manager_stop():
    import subprocess
    import os

    body = {
        "jsonrpc": "2.0",
        "method": "kill",
        "params": signal.SIGTERM,
        "id": 0,
    }
    try:
        req = requests.get(f"http://localhost:{PORT}/process", json=body)
    except requests.exceptions.ConnectionError:
        return 1

    try:
        if req.status_code != 200:
            print(f"ERROR: request returned {req.status_code}")
        else:
            res = req.json()
            if "result" in res:
                for pid in res["result"]:
                    try:
                        # grep for command line arguments and use awk to get
                        # 2nd column of every process that matches
                        output = subprocess.check_output(
                            f"ps -ef | grep '[l]ocalhost {pid}' | awk '{{print $2}}'",
                            shell=True,
                            encoding="utf-8",
                        )
                    except subprocess.CalledProcessError:
                        continue
                    else:
                        pids = [int(x) for x in output.splitlines()]
                        for pid in pids:
                            # Processes that are still alive at this point are
                            # probably zombies/orphans so no need to kill nicely
                            os.kill(pid, signal.SIGKILL)
    finally:
        kill_listener(PORT)

def exec_service_manager_status():
    parser = argparse.ArgumentParser(description="Check if service manager is running")
    parser.add_argument("-s", "--stream", action="store_true", help="Run as stream service")
    args = parser.parse_args()
    if args.stream:
        while True:
            input() # Block until new line
            print(int(not port_empty(PORT)))
    else:
        print(int(not port_empty(PORT)))

def exec_service_status():
    import os
    import requests
    import sys
    import json

    asgmt_path = os.getcwd()
    services = os.path.join(asgmt_path, ".vsce", "services.json")
    if not os.path.exists(services):
        print(f"{os.getcwd()} is not a VSCE Assignment", file=sys.stderr)
        return 1
    
    with open(services, "r") as fp:
        data = json.load(fp)
    
    request_bodies = [
        {
          "jsonrpc": "2.0",
          "id": i,
          "method": "status",
          "params": [(asgmt_path, service["url"]), {}] 
        } for i, service in enumerate(data["services"])
    ]

    req = requests.get(f"http://localhost:{PORT}/status", json=request_bodies)
    if req.status_code != 200:
        print(f"ERROR: request returned {req.status_code}")
        return 2
    response = req.json()
    print(response)
   