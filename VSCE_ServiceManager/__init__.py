import signal
import subprocess
import os
import socket
from typing import Union, Callable, Optional, Tuple
import asyncio
import websockets
import threading
import logging
import pickle


PORT = 4836
LOG_BROADCAST_PORT = 4835

class ValidationError(KeyError): ...

JSON = Union[int, float, str, list, dict, bool, None]
ResolveRequest = Callable[[JSON], dict]
FailedRequest = Callable[[int, str, Optional[JSON]], dict]


def validate_request(data: dict) -> Tuple[ResolveRequest, FailedRequest]:
    """
    Ensure data conforms to JSON-RPC 2.0 specification.
    Args:
        data: A json serializable dictionary
    """
    if data["jsonrpc"] != "2.0":
        raise ValidationError("Invalid request")
    if "method" not in data or "params" not in data:
        raise ValidationError("Invalid request")
    if "id" in data:
        def resolve(result):
            return {
                "jsonrpc": "2.0",
                "id": data["id"],
                "result": result,
            }

        def failure(code, message, _data=None):
            return {
                "jsonrpc": "2.0",
                "id": data["id"],
                "error": {
                    "code": code,
                    "message": message,
                    "data": _data,
                },
            }
        return resolve, failure
    else:
        return lambda *args: "", lambda *args: "" # type: ignore

def kill_listener(port: Union[str, int], signum: int = signal.SIGTERM) -> int:
    """
    Signal the process listening on port.
    Returns:
        0 on success, -1 if no process was found
    """
    output = subprocess.check_output(
        f"lsof -i:{port} | awk '{{print $2}}'", # Get column of PID
        shell=True,
        encoding="utf-8",
    )
    if output:
        os.kill(int(output.split("\n")[1]), signum)
        return 0
    return -1


def port_empty(port) -> int:
    """
    Test whether a process is listening on port
    Returns:
        Non-zero integer if port is empty
    """
    with socket.socket(
        type=socket.SOCK_STREAM,
        family=socket.AF_INET,
    ) as sock:
        try:
            sock.bind(("", int(port)))
        except OSError:
            return 0
        else:
            return 1

class ServiceHandler(logging.Handler):
    """
    Log Handler for use by microservices
    """
    def __init__(self):
        super().__init__()
        self.ws_client: websockets.WebSocketClientProtocol = None
        self.loop = asyncio.new_event_loop()
        
        def thrd_start():
            asyncio.set_event_loop(self.loop)
            async def connection():
                async with websockets.connect(
                    f"ws://localhost:{LOG_BROADCAST_PORT}"
                ) as ws:
                    # Protocol dictates services are ran in
                    # "$ASSIGNMENT_PATH/.vsce/tmp/<service_repo>"
                    # Notify the log service with the assignment path
                    await ws.send(f"service:{os.path.abspath('../../../')}")
                    self.ws_client = ws
                    try:
                        while True:
                            # Keep connection alive
                            await asyncio.sleep(1, loop=self.loop)
                    except KeyboardInterrupt:
                        return

            self.loop.create_task(connection())
            self.loop.run_forever()

        threading.Thread(target=thrd_start, daemon=True).start()
    
    def emit(self, record: logging.LogRecord):
        if self.ws_client is None:
            return
        record.exc_info = None # Can't be pickled
        message = pickle.dumps(record)
        asyncio.run_coroutine_threadsafe(
            self.ws_client.send(message),
            self.loop,
        )
