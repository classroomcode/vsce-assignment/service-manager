from VSCE_ServiceManager import PORT, kill_listener, port_empty, validate_request
from flask import Flask, request, abort
from VSCE_ServiceManager.service import Service
from VSCE_ServiceManager.ws_broadcaster import LogBroadcastService
from typing import Dict, Tuple
import logging
import logging.handlers
import errno
import time

server = Flask(__name__)
logger = logging.getLogger(__name__)
services: Dict[Tuple[str, str], Service] = {}

@server.route("/")
def index(method=["GET"]):
    return ""

@server.route("/process")
def process(method=["GET"]):
    data = request.get_json()
    if data is None:
        logger.error("Failed to parse JSON request")
        abort(400)
    
    resolve, failure = validate_request(data)
    method, params = data["method"], data["params"]
    if method == "start":
        try:
            (path, url, dir, command), kwargs = params
        except:
            logger.warn(f"Invalid parameters")
            return failure(1, "Invalid parameters")
        
        if (path, url) in services:
            services[path, url].incref()
        else:
            services[path, url] = Service(dir, command)
        
        return resolve(services[path, url].port)

    elif method == "stop":
        try:
            (path, url), kwargs = params
        except:
            logger.warn(f"Invalid parameters")
            return failure(1, "Invalid parameters") 
        
        if (path, url) in services:
            service = services[path, url]
            if kwargs["force"]:
                service.terminate(True, on_sucess=lambda:services.pop(path,url))

            elif service.decref() <= 0:
                service.terminate(on_success=lambda: services.pop((path, url)))
    
            return resolve(service.refcnt)
        else:
            logger.warn(f"Service does not exist")
            return failure(3, "Service doesn't exist")
    elif method == "kill":    
        ports = []
        for service in services.values():
            ports.append(service.port)
            kill_listener(service.port)
            while not port_empty(service.port):
                time.sleep(0.25)
        return resolve(ports)
    else:
        return failure(2, f"Invalid method {method}")
            
@server.route("/status")
def status(method=["GET"]):
    data = request.get_json()
    if data is None:
        abort(400)
    
    if isinstance(data, list):
        return [get_status(d, *validate_request(d)) for d in data]  
    else:
        return get_status(data, *validate_request(data))

def get_status(data, resolve, failure):
    method, (args, kwargs) = data["method"], data["params"]
    if method == "status":
        path, url = args
        if (path, url) in services:
            service = services[path, url]
            return resolve([service.port, service.refcnt])
        else:

            return failure(errno.ENODATA, "Service doesn't exit")
    else:
        return failure(errno.ENOSYS, "Method not implemented")


def main():
    logging.getLogger().addHandler(LogBroadcastService())
    server.run("localhost", PORT)

if __name__ == "__main__":
    logging.getLogger().addHandler(logging.StreamHandler())
    main()