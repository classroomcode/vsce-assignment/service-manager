import json
import os
import time
import requests
from VSCE_ServiceManager import PORT, port_empty
import unittest
import subprocess
import VSCE_ServiceManager

class Test_ServiceRequest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.child_proc = subprocess.Popen(["vsce-start"],
            stderr=subprocess.DEVNULL,
            stdout=subprocess.DEVNULL,
        )
        while True:
            try:
                requests.get(f"http://localhost:{PORT}")
            except:
                time.sleep(0.5)
                continue
            else:
                break

    @classmethod
    def tearDownClass(cls):
        cls.child_proc.terminate()
        VSCE_ServiceManager.kill_listener(PORT)

    def test_service_0(self):
        args = [
                "vsce-service-start",
                os.path.abspath("."),
                "https://gitlab.com/classroomcode/minimal-vsce-microservice",
                os.path.abspath(".") + "/tmp/minimal-vsce-microservice",
                f". {os.path.abspath('.')}/env/bin/activate && python3 main.py",
        ]
        output = subprocess.check_output(args,
            encoding="utf-8",
            stderr=subprocess.DEVNULL,
        )
        data = json.loads(output)
        self.assertNotIn("error", data)
        self.assertFalse(port_empty(data["result"]))
        result = data["result"]
        output = subprocess.check_output(args,
            encoding="utf-8",
            stderr=subprocess.DEVNULL,
        )
        data = json.loads(output)
        self.assertEqual(data["result"], result)
        self.port = data["result"]

    def test_service_1(self):
        args = [
            "vsce-service-stop",
            os.path.abspath("."),
            "https://gitlab.com/classroomcode/minimal-vsce-microservice",
        ]
        output = subprocess.check_output(args, encoding="utf-8")
        data = json.loads(output)
        self.assertEqual(data["result"], 1)
        data = json.loads(subprocess.check_output(args, encoding="utf-8"))
        self.assertEqual(data["result"], 0)
        

        


if __name__ == "__main__":
    unittest.main()
