from setuptools import setup, find_packages

setup(
    name="VSCE_ServiceManager",
    version="0.1",
    packages=find_packages(),
    description="Manages service lifetime and port allocation",
    install_requires=[
        "flask",
        "requests",
        "websockets",
    ],
    entry_points={
        "console_scripts":[
            "vsce-service-start=VSCE_ServiceManager.scripts:exec_service_start_request",
            "vsce-service-stop=VSCE_ServiceManager.scripts:exec_service_stop_request",
            "vsce-start=VSCE_ServiceManager.scripts:exec_service_manager",
            "vsce-service-logs=VSCE_ServiceManager.scripts:exec_service_logs",
            "vsce-stop=VSCE_ServiceManager.scripts:exec_service_manager_stop",
            "vsce-status=VSCE_ServiceManager.scripts:exec_service_manager_status",
            "vsce-service-status=VSCE_ServiceManager.scripts:exec_service_status",
        ]
    }
)